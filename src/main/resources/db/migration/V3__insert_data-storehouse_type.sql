
--Tipo de almacenes
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-REG', 'Tipo de almacen regional', NULL, NULL);
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-TRA', 'Tipo de almacen de transito', NULL, NULL);
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-ESTA', 'Tipo de almacen de estacionalidad', NULL, NULL);
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-INT', 'Tipo de almacen de interior', NULL, NULL);
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-EXT', 'Tipo de almacen de exterior', NULL, NULL);
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-DIST-MIN', 'Tipo de almacen de distribucion minorista', NULL, NULL);
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-CONV', 'Tipo de almacen convencional East Theachester', NULL, NULL);
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-INTEL', 'Tipo de almacen inteligente', NULL, NULL);
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-FRIG', 'Tipo de almacen frigorifico', NULL, NULL);
INSERT INTO public.storehouse_type (active, created_date, last_modified_date, code, "name", created_by, last_modified_by) VALUES(true, NULL, NULL, 'TIPO-MED', 'Tipo de almacen medicamentos', NULL, NULL);
