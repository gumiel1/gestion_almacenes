package com.gestion.almacenes.commons.enums;

public enum OrderProductTypeActionEnum {
  RECEIPT, DISPATCH
}
